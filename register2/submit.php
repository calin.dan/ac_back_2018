<?php 

require_once("dbconfig.php");

$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$cnp = "";
$sex = "";
$facebook = "";
$facultate = "";
$birth = "";
$department = "";
$question = "";
$captcha_generated = "";
$captcha_inserted = "";
$check = "";
$error = 0;
$error_text = "";


if(isset($_POST['firstname'])){
	$firstname = $_POST['firstname'];
}

if(isset($_POST['lastname'])){
	$lastname = $_POST['lastname'];
}

if(isset($_POST['phone'])){
	$phone = $_POST['phone'];
}

if(isset($_POST['email'])){
	$email = $_POST['email'];
}

if(isset($_POST['cnp'])){
	$cnp = $_POST['cnp'];
}

if(isset($_POST['facebook'])){
	$facebook = $_POST['facebook'];
}
if(isset($_POST['facultate'])){
	$facultate = $_POST['facultate'];
}

if(isset($_POST['birth'])){
	$birth = $_POST['birth'];
}

if(isset($_POST['department'])){
	$department = $_POST['department'];
}

if(isset($_POST['question'])){
	$question = $_POST['question'];
}

if(isset($_POST['captcha_generated'])){
	$captcha_generated = $_POST['captcha_generated'];
}

if(isset($_POST['captcha_inserted'])){
	$captcha_inserted = $_POST['captcha_inserted'];
}

if(isset($_POST['check'])){
	$check = $_POST['check'];
}


if(empty($firstname) || empty($lastname) || empty($phone) || empty($email) || empty($facebook) || empty($birth) || empty($department) || empty($question) || empty($captcha_inserted) || empty($check) || empty($facultate)){
	$error = 1;
	$error_text = "Sunt campuri necompletate!";
}
if(strlen($firstname) < 3 || strlen($lastname) < 3){
	$error = 1;
	$error_text = "Capurile FIRSTNAME si LASTNAME trebuie sa contina minim 3 caractere";
}
if(strlen($firstname) > 20 || strlen($lastname) > 20){
	$error = 1;
	$error_text = "In capurile FIRSTNAME sau LASTNAME ati depasit 20 de caractere";
}

if(strlen($question)>15){
	$error = 1;
    $error_text = "in campul QUESTION ati intrecut numarul maxim de caractere";
}

if(is_numeric($firstname) || is_numeric($lastname)){
	$error = 1;
	$error_text = "Capurile FIRSTNAME si LASTNAME trebuie sa contina doar litere";
}
if(!is_numeric($phone) || strlen($phone)!=10){
	$error = 1;
	$error_text = "Numarul de telefon nu este valid!";
}


if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {  // se poate si mai bine
   $error = 1;
	$error_text = "Email-ul nu este valid!";
   
} 


if(strlen($cnp)!=13 || !is_numeric($cnp)){
	$error = 1;
	$error_text = "Cnp-ul nu este valid!";
}
if (!preg_match('/^[1-6]/i', $cnp)) { 
   $error = 1;
	$error_text = "Cnp-ul trebue sa aiba prima cifra 1 2 3 4 5 sau 6";
}

if(preg_match('/^[1 3 5]/i',$cnp)){
	$sex="M";
}
if(preg_match('/^[2 4 6]/i',$cnp)){
	$sex="F";
}

if(!preg_match('^facebook\.com\S*[a-zA-Z0-9_]*^', $facebook)){

    $error = 1;
	$error_text = "Link-ul facebook nu este valid!!!";

}
if(is_numeric($facultate) || strlen($facultate) < 3 || strlen($facultate) > 30){
$error = 1;
$error_text = "Camplu facultate nu este valid";
}

$strSystemMaxDate = (date('Y') - 18).'/'.date('m/d'); 
if(strtotime($birth) > strtotime($strSystemMaxDate))
{
    $error = 1;
    $error_text = "Minim 18 ani!!";
}
$validdate = (date('Y')+82).'/'.date('m/d');

if($birth > $validdate){
	$error=1;
	$error_text="Varsta nu poate sa treaca de 100 de ani.";
}


function valid_CNP ($input) 
   {
      for ($i = 0; $i <=12; $i++) 
      {
         $cnp[] = intval($input[$i]);
      }
      
      $suma = $cnp[0] * 2 + $cnp[1] * 7 + $cnp[2] * 9 + $cnp[3] * 1 + $cnp[4] * 4 + $cnp[5] * 6 + $cnp[6] * 3 + $cnp[7] * 5 + $cnp[8] * 8 + $cnp[9] * 2 + $cnp[10] * 7 + $cnp[11] * 9; 
      
      $rest = $suma % 11; 
      
      if (($rest < 10 && $rest == $cnp[12]) || ($rest == 10 && $cnp[12]==1)) 
         $validare = true;
      else 
         $validare = false;
      return $validare;
   }

 function get_birth ($cnp) 
   {
   	$Y= $cnp['1'].$cnp['2'];
       $m= $cnp['3'].$cnp['4'];
      $d = $cnp['5'].$cnp['6'];
      return $Y . '/'.$m .'/'.$d;

   }
   //  $cnpdata=date('Y').'/'.date('m/d');
    /*if($cnpdata!=$birth){
    	$error=1;
    	$error_text="Data nu corespunde cu cnp-ul";
    }*/

if(strlen($captcha_inserted)!=strlen($captcha_generated)){
$error = 1;
	$error_text = "Codul captcha nu este corect!!";

}

try {

    $con = new pdo('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8;', USER, PASSWORD);

} catch(Exception $e) {

    $db_error['connection'] = "Cannot connect to database";

    $response = json_encode($db_error);

    header("HTTP/1.1 503 Service Unavailable");

        echo $response;
    return;

}

/*$mysqli="SELECT*FROM register2 where email='$email'";
$mysqli_result=mysqli_query($mysqli,$con);
if($mysqli_result > 0){
	$error = 1;
	$error_text="mail deja follosit";
}*/

if ($error==0){

$stmt2 = $con -> prepare("INSERT INTO register2(firstname,lastname,phone,email,cnp,sex,facebook,Facultate,birth,department,question) VALUES(:firstname,:lastname,:phone,:email,:cnp,:sex,:facebook,:facultate,:birth,:department,:question)");

$stmt2 -> bindParam(':firstname',$firstname);
$stmt2 -> bindParam(':lastname',$lastname);
$stmt2 -> bindParam(':phone',$phone);
$stmt2 -> bindParam(':email',$email);
$stmt2 -> bindParam(':cnp',$cnp);
$stmt2 -> bindParam(':sex',$sex);
$stmt2 -> bindParam(':facebook',$facebook);
$stmt2 -> bindParam(':facultate',$facultate);
$stmt2 -> bindParam(':birth',$birth);
$stmt2 -> bindParam(':department',$department);
$stmt2 -> bindParam(':question',$question);

if(!$stmt2->execute()){

    $errors['connection'] = "Database Error";

}else{
    echo "Succes";
}
}
else{
	echo $error_text;
}
