-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Gazdă: 127.0.0.1
-- Timp de generare: nov. 04, 2018 la 10:04 AM
-- Versiune server: 10.1.36-MariaDB
-- Versiune PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Bază de date: `ac`
--

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `register`
--

CREATE TABLE `register` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `birthdate` date NOT NULL,
  `department` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `register`
--

INSERT INTO `register` (`id`, `firstname`, `lastname`, `phone`, `email`, `facebook`, `birthdate`, `department`) VALUES
(1, 'Andrei', 'Ion', '0729601114', 'ajgieg@gmail.com', 'wehewhweh', '2017-02-12', 'it'),
(2, 'Dan Adrian', 'Calin', '0721923887', 'calin.danadrian@gmail.com', 'dfasojaois', '2011-11-12', 'ip'),
(3, 'Dan Adrian', 'Calin', '0721923887', 'calin.danadrian@gmail.com', 'dfasojaois', '2011-11-12', 'ip');

-- --------------------------------------------------------

--
-- Structură tabel pentru tabel `register2`
--

CREATE TABLE `register2` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cnp` varchar(255) NOT NULL,
  `Sex` varchar(1) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `Facultate` varchar(30) NOT NULL,
  `birth` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `question` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eliminarea datelor din tabel `register2`
--

INSERT INTO `register2` (`id`, `firstname`, `lastname`, `phone`, `email`, `cnp`, `Sex`, `facebook`, `Facultate`, `birth`, `department`, `question`) VALUES
(1, 'Alexandru', 'Bisag', '0729601114', 'bisagalexstefan@gmail.com', '1234567891234', '', 'facebook.com/bisagalexstefan', '', '1997-07-28', 'it', 'Because'),
(18, 'Dan Adrian', 'Calin', '0721923887', 'calin.danadrian@gmail.com', '1980219394420', 'M', 'https://www.facebook.com/dan.adrian.568', 'CSIE', '1900-02-19', 'it', 'is'),
(21, 'Dan Adrian', 'Calin', '0721923887', 'calin.dn@gmail.com', '1980219394420', 'M', 'https://www.facebook.com/dan.adrian.568', 'CSIE', '1998-12-02', 'it', 'dasd'),
(22, 'Dan Adrian', 'Calin', '0721923887', 'calin.daan@gmail.com', '1980219394420', 'M', 'https://www.facebook.com/dan.adrian.568', 'CSIE', '1998-12-11', 'it', 'das');

--
-- Indexuri pentru tabele eliminate
--

--
-- Indexuri pentru tabele `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id`);

--
-- Indexuri pentru tabele `register2`
--
ALTER TABLE `register2`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_register2` (`email`);

--
-- AUTO_INCREMENT pentru tabele eliminate
--

--
-- AUTO_INCREMENT pentru tabele `register`
--
ALTER TABLE `register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pentru tabele `register2`
--
ALTER TABLE `register2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
